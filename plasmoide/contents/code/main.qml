import QtQuick 2.0                           // Standardkomponent ein alltid må ha
import QtQuick.Controls 2.4 as QtControls    // For bruk av ScrollView
import QtQuick.Layouts 1.1                   // For plassering av alle elementa
import QtWebEngine 1.1                       // For HTML-vising
import org.kde.plasma.core 2.0 as PlasmaCore // For datamotoren som kallar «ordbanken»
import org.kde.plasma.components 3.0 as PlasmaComponents // KDE-alternativ til standardkomponentar

// Hovudvindauge
Item {
    id: rot
    width: 640 // Brei som standard, for å få plass til alle kontrollane
    height: 400
    clip: true // For å unngå at tekst til avkryssingsboksar går utanfor vindauget
    
    // Grupper etter ord og paradigme som standard, og slå opp lemma
    property var arg_ord: "--grupper-ord"
    property var arg_paradigme: "--grupper-paradigme"
    property var arg_type_oppslagsord: "--lemma"
    
    // Køyring av programfila «ordbanken»
    PlasmaCore.DataSource {
        engine: "executable"
        connectedSources: ["ordbanken --html " + arg_ord + " " +
                           arg_type_oppslagsord + " " + 
                           arg_paradigme + " -- '" + 
                           ord.text.replace(/'/g, "’").replace(/ +/g, "' '") + "'"]
        interval: 0 // Oppdater (berre) når «ord.text» vert endra
        
        // Når me får nye søkeresultat (som skjer kvar gong søkefeltet
        // vert oppdatert eller ein endrar grupperingsinnstillingane),
        // oppdater HTML-visinga (eller gøym ho heilt
        // dersom søkefeltet er tomt)
        onNewData:{
            if(ord.text != "") {
                resultat.loadHtml(data.stdout)
                resultatrad.visible = true
            }
            else {
                // Merk at me gøymer HTML-ruta *før* me tømmer ho,
                // då det gjev eit meir responsivt brukargrensesnitt
                resultatrad.visible = false
                resultat.loadHtml("")
            }
        }
    }
    
    // Vis kontrollar og resultat nedover
    ColumnLayout {
        id: hovudkolonne
        width: parent.width
        anchors.left: parent.left
        anchors.right: parent.right
        
            // Søkefelet
            PlasmaComponents.TextField {
                id: ord
                focus: true
                placeholderText: "Skriv inn ordet du vil sjå bøyinga til …"
                clearButtonShown: true   // Vis knapp for å tømma feltet
                Layout.minimumWidth: 200 // Søkefeltet må alltid vera synleg
                Layout.fillWidth: true

            }
            
            // Så ei rad med søke- og avkryssingsboksar
            RowLayout {
                id: knapprad
                spacing: PlasmaCore.Units.mediumSpacing
                
            QtControls.ButtonGroup {
                id: type_oppslagsord
            }
            
            // Tekstforklaring/innleiingstekst til radioknappane
            PlasmaComponents.Label {
                text: "Oppslag:"
            }

            // Val av oppslag på lemma eller bøyingar
            QtControls.RadioButton {
                id: type_oppslagsord_lemma
                text: "Grunnord"
                checked: true
                QtControls.ButtonGroup.group: type_oppslagsord
                onClicked:{
                    if(type_oppslagsord_lemma.checked)
                        arg_type_oppslagsord="--lemma"
                }
            }
            
            QtControls.RadioButton {
                id: type_oppslagsord_boying
                text: "Bøyingar"
                checked: false
                QtControls.ButtonGroup.group: type_oppslagsord
                onClicked:{
                    if(type_oppslagsord_boying.checked)
                        arg_type_oppslagsord="--bøying"
                }
            }
            
            // Lite mellomrom mellom dei to valgruppene
            Item {
                width: PlasmaCore.Units.mediumSpacing
            }
            
            // Tekstforklaring/innleiingstekst til avkryssingsboks
            PlasmaComponents.Label {
                text: "Gruppering:"
            }
            
            // Avkryssingsboks for ordgruppering
            PlasmaComponents.CheckBox {
                id: grupper_ord
                checked: true
                clip: true
                text: "Ord"
                
                // Endra innstillingane når brukaren krysser på/av
                onClicked:{
                    if(grupper_ord.checked)
                        arg_ord="--grupper-ord"
                    else
                        arg_ord="--ikkje-grupper-ord"
                }
            }
            
            // Avkryssingsboks for paradigmegruppering
            PlasmaComponents.CheckBox {
                id: grupper_paradigme
                checked: true
                clip: true
                text: "Paradigme"
                
                // Endra innstillingane når brukaren krysser på/av
                onClicked:{
                    if(grupper_paradigme.checked)
                        arg_paradigme="--grupper-paradigme"
                    else
                        arg_paradigme="--ikkje-grupper-paradigme"
                }
            }
        }
        
        // Rad for vising av søkeresultata
        Row {
            id: resultatrad
            visible: false // Gøym rada før brukaren har søkt

            // Legg til rullefelt om nødvendig
            QtControls.ScrollView {
                width: rot.width
                height: rot.height - knapprad.height - ord.height - 2*hovudkolonne.spacing
                anchors.bottom: parent.bottom
                
                // Vising av søkeresultat som HTML
                WebEngineView {
                    anchors.fill: parent
                    id: resultat
                    zoomFactor: PlasmaCore.Units.devicePixelRatio
                } // Slutt WebEngineView
            } // Slutt ScrollView
        } // Slutt rad
    } // Slutt kolonne
} // Slutt hovudvindauge
