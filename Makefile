# Ordbanken – Skript og fullformsordlister for oppslag i Norsk ordbank.
#
# Copyright © 2008, 2009, 2010, 2011, 2012, 2016 Karl Ove Hufthammer <karl@huftis.org>.
#
#     This file is part of Ordbanken.
#
#     Ordbanken is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Toppmappe for installering (tom som standard):
DESTDIR ?=

# Kva undermappe under DESTDIR ting skal installerast:
PREFIX ?= /usr/local

# Kor ting skal installerast:
INST_PREFIX=$(DESTDIR)$(PREFIX)

# Mappeadressa brukt i skripta frå før:
BUILT_IN_PREFIX=/usr/local

# Kor datafilene skal installerast:
DATA_PREFIX=$(INST_PREFIX)/share/ordbanken

# Kor dokumentasjonsfilene skal installerast:
DOC_PREFIX=$(INST_PREFIX)/share/doc/packages/ordbanken

# Kor plasmoiden skal installerast:
PLASMOID_PREFIX=$(INST_PREFIX)/share/plasma/plasmoids/ordbanken-plasmoid

# Namna på ordlistefilene:
DICT_FILES=$(wildcard fullformsliste_*.txt)
DICT_TO_OPT=$(DICT_FILES:fullformsliste_%.txt=fullform_%.dat)

# Namna på dei ulike tekstfilene som skal installerast:
DATA_FILES=fullform_nb.dat fullform_nn.dat urlkoding.dat
DOC_FILES=norsk_ordbank.pdf gpl-3.0.txt COPYING LESMEG NYTT


# «make» utan argument klargjer ordlistefilene.
all: $(DICT_TO_OPT) urlkoding.dat

# Klargjer ordlistefilene.
fullform_%.dat : fullformsliste_%.txt lemma_%.txt skript/sorter skript/optimer
	@echo
	@echo "Gjer klar ordlistefilene."
	@echo
	@./skript/sorter $@
	@./skript/optimer $@

# Lagar klar URL-kodingsskriptet.
urlkoding.dat : $(DICT_FILES) skript/urlkoding
	@echo
	@echo "Gjer klar URL-kodingsskript."
	@echo
	@./skript/urlkoding

# Installer ordbankskripta og fullformsordlistene:
install: all
	@echo
	@echo "Installerer Ordbanken til mappa «$(INST_PREFIX)»."
	@echo
	@mkdir -p $(INST_PREFIX)/bin
	@echo 'skript/ordbanken -> '$(INST_PREFIX)/bin/ordbanken
	@sed s%$(BUILT_IN_PREFIX)%$(PREFIX)% skript/ordbanken > $(INST_PREFIX)/bin/ordbanken
	@chmod 755 $(INST_PREFIX)/bin/ordbanken
	@mkdir -p $(DATA_PREFIX)
	@for fil in $(DATA_FILES); \
	do\
	  echo $$fil' -> '"$(DATA_PREFIX)/$$fil" ; \
	  cp -f $$fil "$(DATA_PREFIX)/$$fil" ; \
	done
	@mkdir -p $(DOC_PREFIX)
	@for fil in $(DOC_FILES); \
	do\
	  echo $$fil' -> '"$(DOC_PREFIX)/$$fil" ; \
	  cp -f $$fil "$(DOC_PREFIX)/$$fil" ; \
	done
	@mkdir -p $(INST_PREFIX)/share/metainfo/
	@echo 'appdata/ordbanken.appdata.xml -> '$(INST_PREFIX)/share/metainfo/org.huftis.ordbanken.appdata.xml
	@cp -f appdata/ordbanken.appdata.xml "$(INST_PREFIX)/share/metainfo/org.huftis.ordbanken.appdata.xml" 
	@mkdir -p ${DESTDIR}/etc/bash_completion.d
	@echo 'skript/bash_completion_ordbanken -> '${DESTDIR}/etc/bash_completion.d/ordbanken
	@sed s%$(BUILT_IN_PREFIX)%$(PREFIX)% skript/bash_completion_ordbanken > ${DESTDIR}/etc/bash_completion.d/ordbanken
	@echo

# Installer plasmoiden, ikon og andre GUI-ting:
install-kde-gui: ordbanken.desktop plasmoide/metadata.desktop plasmoide/contents/code/main.qml
	@echo
	@echo "Installerer KDE-brukargrensesnitt til mappa «$(PLASMOID_PREFIX)»."
	@mkdir -p $(PLASMOID_PREFIX)/contents/code
	@echo 'plasmoide/metadata.desktop -> '$(PLASMOID_PREFIX)/metadata.desktop
	@cp -f plasmoide/metadata.desktop $(PLASMOID_PREFIX)
	@echo 'plasmoide/contents/code/main.qml -> '$(PLASMOID_PREFIX)/contents/code/main.qml
	@cp -f plasmoide/contents/code/main.qml $(PLASMOID_PREFIX)/contents/code
	@echo 'plasmoide/metadata.desktop -> '$(PLASMOID_PREFIX)/../../services/ordbanken-plasmoid.desktop
	@mkdir -p $(PLASMOID_PREFIX)/../../services
	@cp -f plasmoide/metadata.desktop $(PLASMOID_PREFIX)/../../services/ordbanken-plasmoid.desktop
	@echo 'ordbanken.desktop -> '$(INST_PREFIX)/share/applications/ordbanken.desktop
	@mkdir -p $(INST_PREFIX)/share/applications
	@cp -f ordbanken.desktop $(INST_PREFIX)/share/applications
	@echo "Installerer ikon"
	@mkdir -p $(INST_PREFIX)/share/icons/hicolor
	@chmod 755 $(INST_PREFIX)/share/icons/hicolor
	@find icons -name 'ordbanken.???' | while read fil; \
	do\
	  echo $$fil' -> '"$(INST_PREFIX)/share/$$fil" ; \
		mkdir -p $$(dirname "$(INST_PREFIX)/share/$$fil") ; \
		chmod 755 $$(dirname "$(INST_PREFIX)/share/$$fil") ; \
	  cp -Rf $$fil "$(INST_PREFIX)/share/$$fil" ; \
	done
	@echo

.PHONY test:
test:
	@echo "Køyrer alle einingstestane (skal gje tomt resultat viss OK)."
	@cd testar && ./test-ordbanken.sh
	@cd ..
	
.PHONY check: test

.PHONY clean:
clean:
	@echo
	@echo "Slettar alle .dat-filene (klargjorde ordlistefiler)."
	@rm -f *.dat
	@echo "Slettar alle testresultata."
	@rm -f testar/testresultat/*
	@echo "Slettar eventuelle kompilerte QML-filer."
	@rm -f plasmoide/contents/code/*.qmlc
	@echo

.PHONY releaseclean: clean
.PHONY distclean: clean


# Avinstaller ordbankskripta og fullformsordlistene.
# Sjå til at PREFIX-variabelen er lik som han var
# når du installerte.
uninstall:
	@echo
	@echo "Avinstallerer ordbanken frå «$(INST_PREFIX)»."
	@echo
	@echo "Fjernar programfila $(INST_PREFIX)/bin/ordbanken"
	@rm -f $(INST_PREFIX)/bin/ordbanken
	@echo "Fjernar desktopfila $(INST_PREFIX)/share/applications/ordbanken.desktop"
	@rm -f $(INST_PREFIX)/share/applications/ordbanken.desktop
	@echo "Fjernar autofullføringsskriptet /etc/bash_completion.d/ordbanken"
	@rm -f ${DESTDIR}/etc/bash_completion.d/ordbanken
	@echo "Fjernar datafilene i $(DATA_PREFIX)"
	@rm -rf $(DATA_PREFIX)
	@echo "Fjernar dokumentasjonsfilene i $(DOC_PREFIX)"
	@rm -rf $(DOC_PREFIX)
	@echo "Fjernar plasmoiden i $(PLASMOID_PREFIX)"
	@rm -f $(PLASMOID_PREFIX)/../../services/ordbanken-plasmoid.desktop
	@rm -rf $(PLASMOID_PREFIX)
	@echo "Fjernar appdatafila $(INST_PREFIX)/share/metainfo/org.huftis.ordbanken.appdata.xml"
	@rm -f $(INST_PREFIX)/share/metainfo/org.huftis.ordbanken.appdata.xml
	@echo "Fjernar ikona under $(INST_PREFIX)/share/icons/hicolor"
	@rm -rf $(INST_PREFIX)/share/icons/hicolor/*/*/ordbanken.???
	@rm -rf $(INST_PREFIX)/share/icons/hicolor/*/*/*/*/ordbanken.???
	@echo
