#!/bin/sh
# optimer – Masserer ordlistefilene til eit kjaptsøkt format.
#
# Copyright © 2008, 2009, 2010, 2018–2020 Karl Ove Hufthammer <karl@huftis.org>.
#
#     This file is part of Ordbanken.
#
#     Ordbanken is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Utfør kommandoen på fila oppgjeven som førsteargument.
sluttfil=$1
echo "$sluttfil" # Fila som skal lagast
innfil=${sluttfil%.dat}.txt

# Forklaring på kommandoane:
#  sed:  Gjer programmerings-apostrofar om til ekte apostrofar.
#  gawk: – Fjern «uforståelege» kodar som <intrans2> og <part4/på>, altså 
#         som står mellom vinkelteikn og inneheld tal (<perf-part> o.l.
#         vert ikkje fjerna).
#       – Byt ut mellomrom i paradigmekodane med tabulatorteikn. (Kan ikkje
#         gjerast globalt, på grunn av oppføringar som «på kryss og tvers».)
#       – Skriv til slutt ut informasjonen med oppslagsordet først, så ord-ID,
#         så paradigmekode og til slutt morfologisk skildring.
#  tr: Slå saman etterfølgjande tabulatorar til éin.
#  sort: Sorter fila (slik at oppslag med «look» går raskare).
#
# For forklaring på bruk av «LC_ALL=C» for «sort» her
# (men ikkje for «look» i sjølve ordbanken-fila), sjå
# https://savannah.nongnu.org/bugs/?46362
sed "s/'/’/g" "$innfil" \
| gawk -F'\t' 'BEGIN {OFS="\t" } {
                gsub(/<[^>]*[0-9][^>]*> ?/, "", $4);
                gsub(/ normert/, "", $4);
                gsub(/ /, "\t", $4);
                print $2, $3, $4, $1, $5; }' \
| tr -s '\t' \
| LC_ALL=C sort > "$sluttfil"

# Fjern kjeldefila
rm "$innfil"
