#!/bin/sh

# Fjern eventuelle gamle testresultat
rm -f testresultat/*
mkdir -p testresultat

# Køyr alle testane
while IFS='	' read -r filnamn argument
do
  eval ordbanken "$argument" > testresultat/"${filnamn}".txt
done < testar.dat

# Sjekk om det er forskjellar
diff -q fasit testresultat
