# Ordbanken

## Generell informasjon

Ordbanken er eit brukarvenleg kommandolinjeprogram for
å kjapt slå opp bøyingar av ord på nynorsk og bokmål.

Det støttar regulære uttrykk, autofullføring via
«bash completion» og eksport til HTML-formatet.
Det finst òg eit grafisk brukargrensesnitt.

Datagrunnlaget er fullformsordlistene i Norsk ordbank.
Desse er òg grunnlaget for dei offisielle elektroniske
ordbøkene for nynorsk og bokmål.


## Teknologi

Ordbanken krev stort sett ikkje meir enn eit POSIX-kompatibelt
system. Har du allereie Linux e.l. installert, har du truleg
alle programma du treng. Men for ordens skuld, her er pakkane
du må ha installert (for eit typisk Linux-system):

- grep
- sed
- gawk
- coreutils
- util-linux

Teknisk merknad: I teorien skulle det halda med kva som helst
POSIX-kompatibel versjon av «awk», ikkje nødvendigvis «gawk».
Men det har vist seg at (iallfall per 2018) er det berre «gawk»
som er POSIX-kompatibel nok. Spesielt er det støtte for dei
særnorske bokstavane våre, æøå, som ikkje er korrekt i for eksempel
«mawk» (som blant anna Ubuntu har installert som standard.)
Derfor brukar skripta eksplisitt kommandoen «gawk», og du må
ha dette programmet installert.


## Installering

Last først ned nyaste versjon frå Git-depotet:

```console
git clone https://git.savannah.nongnu.org/git/ordbanken.git
```

Skriv `make` for å klargjera ordlistefilene og
så `make install` (typisk som rotbrukar) for å
installera programmet og ordlistene. Skriv eventuelt
`make install PREFIX=adresse` for å installera i mappa
`adresse` i staden for i `/usr/local`.

For å sjekka at programmet fungerer som det skal
etter at det er installert, køyr `make test`.


## Bruk

Hovudprogrammet heiter `ordbanken`. Viss du berre 
vil slå opp i ordlistene, er dette det einaste 
programmet du treng bruka. Det har innebygd hjelp 
med tips. Skriv `ordbanken --eksempel` for mange
eksempel på både enkel og avansert bruk og for
informasjon om korleis du set opp og brukar
autofullføring med «bash completion».

Mappa «skript» inneheld sjølve hovudprogrammet,
støtte for «bash completion» og skript for å
henta ned og gjera klar ordlistefilene.
Du treng vanlegvis ikkje gjera noko der.

Viss du brukar KDE/Plasma, kan du installera eit grafisk
brukargrensesnitt og eit såkalla «skjermelement»
(ein «widget»). Skriv `make install-kde-gui` (typisk som
rotbrukar), eventuelt `make install-gui PREFIX=adresse`.
Avslutt så med `kbuildsycoca5` (som vanleg brukar).
No skal programmet vera tilgjengeleg i programmenyen
og i skjermelementmenyen. Sistenemnde finn du ved å
høgreklikka på skrivebordet og velja «Legg til 
element».


## Nettsider

Heimeside for programmet, med ei kort innføring
og mykje informasjon:  
https://huftis.org/artiklar/ordbanken/

Prosjektside for programmet, der du blant anna
kan melda frå om feil og melda deg på e-postlista:  
https://savannah.nongnu.org/projects/ordbanken/

Ferdigbygde pakkar for Linux-distribusjonar:

- openSUSE: https://software.opensuse.org/package/ordbanken
- Gentoo: https://github.com/hansfn/gentoo-overlay

Fullformsordlistene frå Norsk ordbank er (i lettare
tilpassa utgåve) inkludert i programmet, og eg vil
oppdatera ordlistefilene når Norsk ordbank legg ut
nye versjonar. Her er originalfilene:  
https://www.nb.no/sprakbanken/ressurskatalog/oai-nb-no-sbr-41/
https://www.nb.no/sprakbanken/ressurskatalog/oai-nb-no-sbr-5/


## Opphavspersonar og bidragsytarar

Programmet er utvikla og vert vedlikehalde av
Karl Ove Hufthammer <karl@huftis.org>.

Fullformsordlistene vert vedlikehaldne av
Språksamlingane ved Universitetet i Bergen
og av Språkrådet.

Sjå fila `COPYING` for informasjon om opphavsrett og
lisensvilkår.

Takk til desse for bidrag med programkode eller
distropakking:

- Egil Bonarjee
- Ketil Kvifte
- Hans Fredrik Nordhaug

Takk òg til dei som har meldt frå om feil:  
https://savannah.nongnu.org/bugs/?group=ordbanken


                 Karl Ove Hufthammer <karl@huftis.org>
