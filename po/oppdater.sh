#!/bin/sh
# oppdater.sh – Oppdater omsettingsfiler og kopier omsettingar til filer som brukar dei.
#
# Copyright © 2016, 2020 Karl Ove Hufthammer <karl@huftis.org>.
#
#     This file is part of Ordbanken.
#
#     Ordbanken is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
intltool-update --pot --gettext-package=ordbanken
intltool-update --gettext-package=ordbanken nn
intltool-update --gettext-package=ordbanken nb
cd ..
intltool-merge --xml-style po appdata/ordbanken.appdata.xml.in appdata/ordbanken.appdata.xml
intltool-merge --desktop-style po ordbanken.desktop.in ordbanken.desktop
cd - || exit
